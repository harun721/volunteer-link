@extends('master')

@section('content')
    <a href="#" class="btn">Yohanens</a>

    {!! Form::model($song, ["url" => "songs/" . $song->slug, "method" => "PATCH"]) !!}
            <div class="form-group">
                {!! Form::text("title", null, ["class" => "form-control"]) !!}
            </div>
            <div class="form-group">
                {!! Form::textarea("lyrics", null, ["class" => "form-control"]) !!}

            </div>
            <div class="form-group">
                {!! Form::submit("Update Song", ["class" => "btn btn-primary"]) !!}
            </div>



    {!! Form::close() !!}
@stop