@extends('master')

@section('content')

   <h1> {{$song->title}} </h1>

    @if($song->lyrics)
        <article>
            {!! nl2br($song->lyrics)!!}
        </article>
    @endif
@stop