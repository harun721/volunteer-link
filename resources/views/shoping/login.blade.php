<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Volunteer link</title>
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/form-elements.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

</head>

<body>

<!-- Top content -->
<div class="top-content">
    <div class="container">

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
                <h1>Volunteer Link</h1>
                <div class="description">
                    <P>Login / Register with us !</P>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 show-forms">
                <span class="show-register-form">Register</span>
                <span class="show-forms-divider">/</span>
                <span class="show-login-form active">Login</span>
            </div>
        </div>
        <h1>Type: {{ $display}}</h1>
        <div class="row register-form" style="display: none;">
            <div class="col-sm-4 col-sm-offset-1">
                <form role="form" action="{{url('register')}}" method="post" class="r-form">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="sr-only" for="r-form-first-name">Full name</label>
                        <input type="text" name="name" placeholder="User name" class="r-form-first-name form-control" id="r-form-first-name">
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label class="sr-only" for="r-form-last-name">Last name</label>--}}
                        {{--<input type="text" name="r-form-last-name" placeholder="Last name..." class="r-form-last-name form-control" id="r-form-last-name">--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label class="sr-only" for="r-form-user-name">Choose your username</label>--}}
                        {{--<input type="text" name="r-form-user-name" placeholder="Username..." class="r-form-user-name form-control" id="r-form-user-name">--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label class="sr-only" for="r-form-email">Email</label>
                        <input type="text" name="email" placeholder="Email" class="r-form-email form-control" id="r-form-email">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="r-form-password">Password</label>
                        <input type="password" name="password" placeholder="Password" class="r-form-password form-control" id="r-form-password">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="r-form-password">Confirm Password</label>
                        <input type="password" name="r-form-password" placeholder="Confirm Password..." class="r-form-password form-control" id="r-form-confirmpassword">
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label class="sr-only" for="r-form-about-yourself">About yourself</label>--}}
                        {{--<textarea name="r-form-about-yourself" placeholder="About yourself..."--}}
                                  {{--class="r-form-about-yourself form-control" id="r-form-about-yourself"></textarea>--}}
                    {{--</div>--}}
                    <button type="submit" class="btn">Sign me up!</button>
                </form>


            </div>
            <div class="col-sm-6 forms-right-icons">
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-pencil"></i></div>
                    <div class="col-sm-10">
                        <h3>Register with us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-commenting"></i></div>
                    <div class="col-sm-10">
                        <h3>About us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-magic"></i></div>
                    <div class="col-sm-10">
                        <h3>Contact us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row login-form" style="display: block;">
            <div class="col-sm-4 col-sm-offset-1">
                <form method="POST" action="{{url('auth/login')}}" class="l-form">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="sr-only" for="l-form-username">Username</label>
                        <input type="email" name="email"  placeholder="Email Address" class="l-form-username form-control" id="l-form-username"/>

                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="l-form-password">Password</label>
                        <input type="password" name="password"  placeholder="Password" class="l-form-password form-control" id="l-form-password" />
                    </div>
                    <button type="submit" class="btn">Sign in!</button>
                </form>
                <div class="social-login">
                    <p>Or login with:</p>
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="btn btn-link-1" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="btn btn-link-1" href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 forms-right-icons">
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-user"></i></div>
                    <div class="col-sm-10">
                        <h3>New Features</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-eye"></i></div>
                    <div class="col-sm-10">
                        <h3>Easy To Use</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 icon"><i class="fa fa-twitter"></i></div>
                    <div class="col-sm-10">
                        <h3>Social Integrated</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">

            <div class="col-sm-8 col-sm-offset-2">
                <div class="footer-border"></div>
            </div>

        </div>
    </div>
</footer>

<!-- Javascript -->
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.backstretch.min.js"></script>
<script src="/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="/js/placeholder.js"></script>
<![endif]-->

</body>

</html>