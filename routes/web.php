<?php


Route::get('songs', "SongsController@index");
Route::get("songs/{slug}", "SongsController@show");
Route::get("songs/{slug}/edit", "SongsController@edit");
Route::patch("songs/{slug}", "SongsController@update");


Route::get(/**
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */
    "blade", function(){
    return view('shoping.page');
});

Route::get("/", "Front@login");
//Route::get("/products", "Front@products");
Route::get("/products/details/{id}", "Front@productDetails");
Route::get("/products/catagories", "Front@productCatagoryies");
Route::get("/products/brands", "Front@productBrands");
Route::get("/blog", "Front@blog");
Route::get("/blog/post/{id}", "Front@blogPost");
Route::get("/contact-us", "Front@contactUs");
Route::get("/login", "Front@login");
Route::get("/logout", "Front@logout");
Route::get("/cart", "Front@cart");
Route::get("/checkout", ["middleware" => "auth", "uses" => "Front@checkout"]);
Route::get("/search/{query}", "Front@search");


Route::post("/product", "Front@cart");

Route::get('auth/login', 'Front@login');
Route::post('auth/login', 'Front@authenticate');
Route::get('auth/logout', 'Front@logout');

// Registration routes...
Route::post('/register', 'Front@register');

Route::get("/products/{id?}", ['middleware' => "auth.basic", function($id = null){
    if($id == null){
        $products = \App\Product::all(array('id', 'name', 'price'));

    }
    else{
        $products = \App\Product::find($id, array('id', 'name', 'price'));
    }
    return Response::json(array(
        'error' => false,
        'products' => $products,
        'status_code' => 200
    ));
}]);

Route::get("/categories/{id?}", ["middleware" => "auth.basic", function($id = null){
    if($id == null){
        $categories = App\Category::all(array('id', 'name'));
        $categories = App\Category::find($id, array('id', 'name'));
    }
    else{
        $categories = App\Category::find($id, array('id', 'name'));
    }
    return Response::json(array(
       'error' => false,
        "user" => $categories,
        'status_code' => 200,
    ));
}]);