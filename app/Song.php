<?php namespace App;
use Illuminate\Database\Eloquent\Model;
class Song extends Model{
    /**
     * Fillabe fields for a song
     * @var array
     */
    public $fillable = [
        "title",
        "lyrics",
    ];
}