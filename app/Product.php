<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = ['name', "title" , "description" , "price", "category_id", "brand_id", "created_at_ip", "updated_at_ip"];
    protected $primaryKey = "id";
}
