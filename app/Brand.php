<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brands";
    protected $fillable = ['name', "created_at_ip", "updated_at_ip"];
    protected $primaryKey = "id";
}
