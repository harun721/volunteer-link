<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
     public function checkAuth(Request $request){
         $credentials = [
             'email' => $request->input('email'),
             'password' => $request->input('password'),
         ];

         if(!\Auth::attempt($credentials)){
             return response('User name and password does not match', 403);
         }
         return response(\Auth::user(), 201);
     }
}
