<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Support\Facades\Request;
use App\User;
use Illuminate\Support\Facades\Auth;





class Front extends Controller
{

    var $brands;
    var $products;
    var $title;
    var $description;
    var $categories;

    public function __construct()
    {
        $this->brands = Brand::all(array('name'));
        $this->categories = Category::all(array('name'));
        $this->products = Product::all(array('id', 'name', 'price'));
    }

    public function index()
    {
        return view('shoping.home', array('title' => 'Welcome', 'description' => '', 'page' => 'home', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function products()
    {
        return view('shoping.products', array('title' => 'Products Listing', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function product_details($id)
    {
        $product = Product::find($id);
        return view('shoping.product_details', array('product' => $product, 'title' => $product->name, 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function product_categories($name)
    {
        return view('shoping.products', array('title' => 'Welcome', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function product_brands($name, $category = null)
    {
        return view('shoping.products', array('title' => 'Welcome', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function blog()
    {
        return view('shoping.blog', array('title' => 'Welcome', 'description' => '', 'page' => 'blog', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function blog_post($id)
    {
        return view('shoping.blog_post', array('title' => 'Welcome', 'description' => '', 'page' => 'blog', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
    }

    public function contact_us()
    {
        return view('shoping.contact_us', array('title' => 'Welcome', 'description' => '', 'page' => 'contact_us'));
    }

    public function login()
    {
        return view('shoping.login', array('title' => 'Welcome', 'description' => '', 'page' => 'home', 'display' => "login"));
    }

    public function logout()
    {
        Auth::logout();
        return \Redirect::away("login");
        return view('shoping.login', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
    }

    public function cart()
    {
        //update/ add new item to cart
        if (Request::isMethod('post')) {
            $product_id = Request::get('product_id');
            $product = Product::find($product_id);
            Cart::add(array('id' => $product_id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price));
        }

        //increment the quantity
        if (Request::get('product_id') && (Request::get('increment')) == 1) {
            $rowId = Cart::search(array('id' => Request::get('product_id')));

            $item = Cart::get($rowId[0]);

            Cart::update($rowId[0], $item->qty + 1);
        }

        //decrease the quantity
        if (Request::get('product_id') && (Request::get('decrease')) == 1) {
            $rowId = Cart::search(array('id' => Request::get('product_id')));
            $item = Cart::get($rowId[0]);

            Cart::update($rowId[0], $item->qty - 1);
        }


        $cart = Cart::content();

        return view('cart', array('cart' => $cart, 'title' => 'Welcome', 'description' => '', 'page' => 'home'));
    }


    public function checkout()
    {
        return view('shoping.checkout', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
    }

    public function search($query)
    {
        return view('shoping.products', array('title' => 'Welcome', 'description' => '', 'page' => 'products'));
    }

    public function register()
    {
        if (Request::isMethod('post')) {
            User::create([
                "name" => Request::get("name"),
                "email" => Request::get('email'),
                "password" => bcrypt(Request::get("password")),
            ]);

        }

        return \Redirect::away("login");
    }

    public function authenticate()
    {
        if (Auth::attempt(['email' => Request::get('email'), 'password' => Request::get("password")])) {
            return redirect()->intended('checkout');
        } else {
            return view("shoping.login", ["title" => "Welcome", "description" => "", "page" => 'home', "loginSuccess" => 0]);
        }
    }
}
