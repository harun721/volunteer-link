<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;

use App\Http\Requests;

class SongsController extends Controller
{
    //
    private $song;
    public function __construct(Song $song){
        $this->song = $song;
    }

    public function index(){

        $songs = $this->song->get();
        return view("songs.index")->with("songs", $songs);
    }
    public function show($slug){

        $song = $this->song->where("slug", $slug)->first();
        return view("songs.showSong")->with("song", $song);
    }
    public function getSongs(){


        $songs = ["sorry", "if i were your boy friend", "baby"];


        return $songs;
    }
    public function edit($slug){
        $song = $this->song->where("slug", $slug)->first();
        return view("songs.edit")->with("song", $song);
    }
    public function update($slug, Request $request){
        $song = $this->song->where("slug", $slug)->first();
        $song->fill(["title" => $request->get('title'), "lyrics" => $request->get('lyrics')])->save();

        return redirect("songs");

    }
}
