<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    //
    public function index(){
        $lessons = ["First lesson", "second lesson", "third lesson"];
        return view('pages.home',compact(""));
    }
    public function about(){
        return view("pages.about");
    }
}
