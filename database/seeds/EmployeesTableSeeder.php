<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $counter = 10;


        for($i = 0; $i < $counter; $i++){
            $user = new \App\Employee();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->contact_number = $faker->phoneNumber;
            $user->save();
        }

    }
}
